<?php
	require_once 'config.php';
	require_once 'alphaID.inc.php';
	require_once 'shared.php';
	$is_3ds = strpos( $_SERVER['HTTP_USER_AGENT'], 'Nintendo 3DS' );

	if( isset( $_GET['imageid'] ) || !empty( $_GET['imageid'] )  || isset( $_GET['deletekey'] ) || !empty( $_GET['deletekey'] )) {
		$photo_id = alphaID( $_GET['imageid'], true,4 );
		$deletekey = $_GET['deletekey'];
		$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
		$stmt = $db->prepare( 'DELETE FROM photos WHERE id = ? AND deletekey = ?' );
		$stmt->bind_param( 'is', $photo_id, $deletekey );
		if($stmt->execute()) {
			if($stmt->affected_rows > 0) {
				echo '<p class="alert alert-success">Your image has been deleted!</p>';
				delete_ac_image( $_GET['imageid'] );
			} else {
				echo '<div class="alert alert-error">There was an error deleting your file!</div>';
			}
		}
		$stmt->close();
		$db->close();
	}
?>
<!doctype html>
<html lang="en">
<head>
<title>Animal Crossing image uploader</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="img/favicon.ico" type="image/x-icon">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="css/jasny-bootstrap.min.css" rel="stylesheet">
<link href="css/jasny-bootstrap-responsive.min.css" rel="stylesheet">
<link href="css/acimages.css" rel="stylesheet">
</head>
<body>
<div class="container">
<div class="row">
<div class="span12">
<div class="content">
<div class="top-bar">
<a title="Home" href="<?php echo SITE_URL; ?>"><img class="logo" title="ACimages" width=80 height=27 alt="ACimages" src="<?php echo SITE_URL; ?>img/logo-small.png"></a>
<ul class="nav nav-ac nav-pills nav-pills-ac">
<li><a href="<?php echo SITE_URL; ?>user.php">Find user</a></li>
<li class="active"><a href="<?php echo SITE_URL; ?>delete.php">Delete</a></li>
<?php if( !$is_3ds ) : ?>
<li><a target="_blank" href="<?php echo SITE_URL; ?>feed.rss">RSS</a></li>
<?php endif; ?>
</ul>
<?php if( !$is_3ds ) { include 'donate.inc.php'; } ?>
<div class="clearfix"></div>
</div>
<form class="form-inline" action="" method="POST">
<input type="text" class="input-small" placeholder="Image ID" name="imageid" value="<?php if( isset( $_GET['imageid'] ) ) echo $_GET['imageid']; ?>">
<input type="text" placeholder="Delete Key" name="deletekey" value="<?php if( isset( $_GET['deletekey'] ) ) echo $_GET['deletekey']; ?>">
<button type="submit" class="btn btn-danger">Delete</button>
</form>
</div>
</div>
</div>
</div>
</body>
</html>