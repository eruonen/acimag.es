<?php
require_once 'config.php';
require_once 'alphaID.inc.php';
$is_3ds = strpos( $_SERVER['HTTP_USER_AGENT'], 'Nintendo 3DS' );
$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
$stmt = $db->prepare( 'SELECT `id`, `uploader`, `timestamp` FROM photos WHERE id = ?' );
$photo_id = alphaID( $_GET['id'], true, 4 );
$stmt->bind_param( 'i', $photo_id );
$stmt->execute();
$stmt->bind_result( $photo_id, $uploader, $timestamp );
$fetched = $stmt->fetch();
$stmt->close();
?>
<!doctype html>
<html lang="en">
<head>
<?php if( $fetched ) : ?>
<title>Image by <?php echo htmlentities( $uploader, ENT_QUOTES ); ?></title>
<?php else: ?>
<title>Could not find image</title>
<?php endif; ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="<?php echo SITE_URL; ?>img/favicon.ico" type="image/x-icon">
<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/jasny-bootstrap.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/jasny-bootstrap-responsive.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/acimages.css" rel="stylesheet">
</head>
<body>
<div class="container">
<div class="row">
<div class="span12">
<div class="content">
<div class="top-bar">
<a title="Home" href="<?php echo SITE_URL; ?>"><img class="logo" title="ACimages" width=80 height=27 alt="ACimages" src="<?php echo SITE_URL; ?>img/logo-small.png"></a>
<ul class="nav nav-ac nav-pills nav-pills-ac">
<li><a href="<?php echo SITE_URL; ?>user.php">Find user</a></li>
<li><a href="<?php echo SITE_URL; ?>delete.php">Delete</a></li>
<?php if( !$is_3ds ) : ?>
<li><a target="_blank" href="<?php echo SITE_URL; ?>feed.rss">RSS</a></li>
<?php endif; ?>
</ul>
<?php if( !$is_3ds ) { include 'donate.inc.php'; } ?>
<div class="clearfix"></div>
</div>
<?php 
if( $fetched ) :
	if( isset( $photo_id ) && !is_null( $photo_id ) ) : ?>
	<img width=400 height=240 title="<?php echo $_GET['id']; ?>.jpg uploaded by <?php echo $uploader; ?>" src="<?php echo SITE_URL; ?>i/<?php echo $_GET['id']; ?>.jpg"><br>
	<p><small><strong><?php echo $_GET['id']; ?></strong>.jpg by <?php echo htmlentities( $uploader, ENT_QUOTES ); ?> on <?php echo $timestamp; ?>.</small></p>
	<?php else: ?>
	<div class="alert alert-error">Could not find image specified.</div>
	<?php endif; ?>
	<?php echo '<h5>Other recent uploads by <a href="' . SITE_URL . 'user.php?user=' . htmlentities( $uploader, ENT_QUOTES ) . '">' . htmlentities( $uploader, ENT_QUOTES ) . '</a></h5>'; ?>
	<div class="preview">
	<?php
	$stmt = $db->prepare( 
		'SELECT `id`, `uploader`, `timestamp` 
		FROM photos 
		WHERE `uploader` = ? 
		AND NOT `id` = ? 
		ORDER BY  `timestamp` DESC 
		LIMIT 0 , 6'
	);
	$stmt->bind_param( 'si', $uploader, $photo_id );
	$stmt->execute();
	$stmt->bind_result( $photo_id, $uploader, $timestamp );
	while( $stmt->fetch() ){
		$filename = alphaID( $photo_id, false, 4 );
		$image_page = SITE_URL . 'i/' . $filename . '/';
		$image_url = SITE_URL . 'i/' . $filename . '.jpg';
	    echo '<a href="' . $image_page . '"><img width=129 height=77 src="' . $image_url . '"></a>';
	}
	$stmt->close();
	$db->close();
	?>
	</div>
<?php else: ?>
	<div class="alert alert-error">Could not find image specified.</div>
<?php endif; ?>
</div>
</div>
</div>
</div>
</body>
</html>
