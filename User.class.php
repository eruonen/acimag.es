<?php 

require_once 'config.php';

class User {

	public function __construct( $username ) {

		$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
		$this->username = $username;
		$stmt = $db->prepare( 'SELECT count(id) 
			FROM photos 
			WHERE `uploader` = ?' );
		$stmt->bind_param( 's', $this->username );
		$stmt->execute();
		$stmt->bind_result( $this->num_images );
		$stmt->fetch();
		if( $this->has_images = $this->num_images > 0 ) {
			$this->num_pages = ceil( $this->num_images / 10 );
		} else {
			$this->num_images = 0;
			$this->num_pages = 0;
		}
		$stmt->close();
		$db->close();

	}

	public function get_images() {

		if( $this->has_images ) {

			$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
			$stmt = $db->prepare( 'SELECT `id`
				FROM  photos 
				WHERE `uploader` = ?
				ORDER BY  `timestamp` DESC' );
			$stmt->bind_param( 's', $this->username );
			$stmt->execute();
			$stmt->bind_result( $image );
			while ( $stmt->fetch() ) {
				$images[] = $image;
			}
			$stmt->close();
			$db->close();
			if ( !empty( $images ) ) {
				return $images;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function get_image_page( $page, $limit = 10 ) {

		if( $this->has_images ) {

			$page = ( $page - 1 ) * $limit;

			$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
			$stmt = $db->prepare( 'SELECT `id`
				FROM photos
				WHERE `uploader` = ?
				ORDER BY `timestamp` DESC
				LIMIT ? , 10' );
			$stmt->bind_param( 'si', $this->username, $page );
			$stmt->execute();
			$stmt->bind_result( $image );
			while ( $stmt->fetch() ) {
				$images[] = $image;
			}
			$stmt->close();
			$db->close();
			if ( !empty( $images ) ) {
				return $images;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
?>