<?php 
	require_once( 'config.php' ); 
	require_once 'alphaID.inc.php';
	require_once 'php_fast_cache.php';
	$is_3ds = strpos( $_SERVER['HTTP_USER_AGENT'], 'Nintendo 3DS' );
?>
<!doctype html>
<html lang="en">
<head>
<title>Animal Crossing image uploader</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="img/favicon.ico" type="image/x-icon">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="css/jasny-bootstrap.min.css" rel="stylesheet">
<link href="css/jasny-bootstrap-responsive.min.css" rel="stylesheet">
<link href="css/acimages.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="js/jasny-bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<div class="row">
<div class="span12">
<div class="content">
<div class="top-bar">
<a title="Home" href="<?php echo SITE_URL; ?>"><img class="logo" title="ACimages" width=80 height=27 alt="ACimages" src="<?php echo SITE_URL; ?>img/logo-small.png"></a>
<ul class="nav nav-ac nav-pills nav-pills-ac">
<li><a href="user.php">Find user</a></li>
<li><a href="delete.php">Delete</a></li>
<?php if( !$is_3ds ) : ?>
<li><a target="_blank" href="feed.rss">RSS</a></li>
<?php endif; ?>
</ul>
<?php if( !$is_3ds ) { include 'donate.inc.php'; } ?>
<div class="clearfix"></div>
</div>
<?php if( $is_3ds || DEBUG ) : ?>
<form id="upload-img" method="post" enctype="multipart/form-data" action="upload.php">
<div class="input-prepend">
<span class="add-on"><i class="icon-user"></i></span>
<input id="nick" name="nick" type="text" maxlength=45 placeholder="Your nick" value="<?php if( isset( $_COOKIE['nick'] ) ) echo $_COOKIE['nick']; ?>" required>
</div>
<div id="upload1" class="fileupload fileupload-new" data-provides="fileupload"><span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input name="images[]" type="file" /></span> &nbsp;<span class="fileupload-preview"></span></div>
<div id="upload2" style="display:none;" class="fileupload fileupload-new" data-provides="fileupload"><span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input name="images[]" type="file" /></span> &nbsp;<span class="fileupload-preview"></span></div>
<div id="upload3" style="display:none;" class="fileupload fileupload-new" data-provides="fileupload"><span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input name="images[]" type="file" /></span> &nbsp;<span class="fileupload-preview"></span></div>
<div id="upload4" style="display:none;" class="fileupload fileupload-new" data-provides="fileupload"><span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input name="images[]" type="file" /></span> &nbsp;<span class="fileupload-preview"></span></div>
<p id="show-next-upload"><a href="#" onclick="showNextUpload();">Upload another file</a></p>
<input id="upload-submit" class="btn btn-ac fileupload-exists" type="submit" value="Upload image!">
</div>
<div id="no-nick-error" style="display:none;" class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>Please enter a nickname before uploading!</div>
</form>
<?php else : ?>
<?php if( !empty( $_GET['error'] ) ) : ?>
<div class="alert alert-error">Please recheck your Image Code. A valid example: Bu77</div>
<?php endif; ?>
<p>Welcome to acimag.es! If you wish to upload images, please do so from your 3DS browser!</p><br>
<form class="form-inline" method="get" action="redirect.php">
<input type="text" class="input-small" placeholder="Image Code" name="image">
<button type="submit" class="btn btn-ac">Go to image</button>	
</form>
<?php
	$rand_html = phpFastCache::get( 'rimages' );
	if ( $rand_html == null ) {
		$rand_images = array();
		$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
		$stmt = $db->prepare( 'SELECT `id`
			FROM  photos 
			ORDER BY  `id` ASC' );
		$stmt->execute();
		$stmt->bind_result( $image_id );
		while ( $stmt->fetch() ) {
			$rand_images[] = $image_id;
		}
		$stmt->close();
		$db->close();
		phpFastCache::set( 'randimages', $rand_images, 60 );

		if ( !empty( $rand_images[0] ) ) {
			$rand_html =  '<div style="max-width:900px;"><h5>Some random user images</h5>';
			for ( $i = 0; $i < 4; $i++ ) {
				$image = rand( 0, count( $rand_images ) - 1 );
				$image = $rand_images[$image];
				$image = alphaID( $image, false, 4 );
				$rand_html .= '<a href="' . SITE_URL . 'i/' . $image . '/"><img width=400 height=240 style="margin:5px;" title="' . $image . '.jpg" src="' . SITE_URL. 'i/' . $image . '.jpg"></a>';
			}
			$rand_html .= '</br>Generated at ' . date( 'g:ia T' ) . '</div>';
		}
		phpFastCache::set( 'rimages', $rand_html, 60 );
	}

	echo $rand_html;
?>
<?php endif; ?>
</div>
</div>
</div>
</div>
<script>$("#upload-img").submit(function(a){if($("#nick").val().length==0){$("#no-nick-error").show();return false}});var file=1;function showNextUpload(){if(++file<=4){$("#upload"+file).show();$("#upload-submit").val("Upload images!")}if(file===4){$("#show-next-upload").hide()}};</script>
</body>
</html>