<?php
	require_once 'config.php';
	require_once 'alphaID.inc.php';
	require_once 'User.class.php';
	require_once 'Pagination.class.php';
	$is_3ds = strpos( $_SERVER['HTTP_USER_AGENT'], 'Nintendo 3DS' );
?>
<!doctype html>
<html lang="en">
<head>
<title>Animal Crossing image uploader</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="<?php echo SITE_URL; ?>img/favicon.ico" type="image/x-icon">
<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/jasny-bootstrap.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/jasny-bootstrap-responsive.min.css" rel="stylesheet">
<link href="<?php echo SITE_URL; ?>css/acimages.css" rel="stylesheet">
</head>
<body>
<div class="container">
<div class="row">
<div class="span12">
<div class="content">
<div class="top-bar">
<a title="Home" href="<?php echo SITE_URL; ?>"><img class="logo" title="ACimages" width=80 height=27 alt="ACimages" src="<?php echo SITE_URL; ?>img/logo-small.png"></a>
<ul class="nav nav-ac nav-pills nav-pills-ac">
<li><a href="<?php echo SITE_URL; ?>user.php">Find user</a></li>
<li><a href="<?php echo SITE_URL; ?>delete.php">Delete</a></li>
<?php if( !$is_3ds ) : ?>
<li><a target="_blank" href="<?php echo SITE_URL; ?>feed.rss">RSS</a></li>
<?php endif; ?>
</ul>
<?php if( !$is_3ds ) { include 'donate.inc.php'; } ?>
<div class="clearfix"></div>
</div>
<form class="form-inline" action="">
<div class="input-prepend">
<span class="add-on"><i class="icon-search"></i></span>
<input id="user" name="user" type="text" maxlength=45 placeholder="Username" value="<?php if( isset( $_GET['user'] ) ) echo $_GET['user']; ?>" required>
</div>
<button type="submit" class="btn btn-ac">Search</button>
</form>
<?php if ( isset( $_GET['user'] ) && !empty( $_GET['user'] ) ) {
	if ( isset($_GET['page']) && !empty( $_GET['page'])) {
		$currentpage = $_GET['page'];
	} else {
		$currentpage = 1;
	}
	$currentuser = new User( $_GET['user'] );
	echo '<h4>Images by ' . htmlentities( $_GET['user'], ENT_QUOTES ) . '</h4>';
	echo '<div style="max-width:900px;">';	
	if ( $currentuser->has_images ) {
		var_dump($currentuser->get_image_page( $currentpage ));
		foreach ( $currentuser->get_image_page( $currentpage ) as $image ) { 
			$image = alphaID( $image, false, 4 );
			echo '<a href="' . SITE_URL . 'i/' . $image . '/"><img width=400 height=240 style="margin:5px;" title="' . $image . '.jpg uploaded by ' . htmlentities( $_GET['user'], ENT_QUOTES ) . '" src="' . SITE_URL. 'i/' . $image . '.jpg"></a>'; 
		}
		unset($image);
		echo '<div class="pagination pagination-large">';
		echo Pagination::generate_pagination( $currentpage, $currentuser->num_pages, 10, SITE_URL . 'user.php?user=' . $_GET['user'] );
		echo '</div>';
	} else {
		echo '<div class="alert alert-error">User not found.</div>';
	}
}
?>
</div>
</div>
</div>
</div>
</body>
</html>