<?php
	require_once 'config.php';
	require_once 'alphaID.inc.php';
	require_once 'rss_generator.php';
	require_once 'php_fast_cache.php';

	$rss = phpFastCache::get( 'rss' );

	if ( $rss == null ) {

		$rss_channel = new rssGenerator_channel();
		$rss_channel->atomLinkHref = 'http://acimag.es/feed.rss';
		$rss_channel->title = 'Animal Crossing Images';
		$rss_channel->link = 'http://acimag.es/feed.rss';
		$rss_channel->description = 'The latest pics about animal crossing.';
		$rss_channel->language = 'en-us';
		$rss_channel->generator = 'PHP RSS Feed Generator';
		$rss_channel->managingEditor = '';
		$rss_channel->webMaster = '';

		$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
		if(isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {
			$stmt = $db->prepare( 
				'SELECT `id`, `uploader`, `timestamp` 
				FROM photos 
				WHERE uploader = ?
				ORDER BY  `timestamp` DESC 
				LIMIT 0 , 20'
			);
			$stmt->bind_param( 's', $_REQUEST['user'] );
		} else {
			$stmt = $db->prepare( 
				'SELECT `id`, `uploader`, `timestamp` 
				FROM photos 
				ORDER BY  `timestamp` DESC 
				LIMIT 0 , 20'
			);
		}
			$stmt->execute();
			$stmt->bind_result( $photo_id, $uploader, $timestamp );
		while( $stmt->fetch() ){
			$timestamp = date('r', strtotime($timestamp));
			$file = SITE_URL . 'i/' . alphaID( $photo_id, false, 4 );
			$item = new rssGenerator_item();
			$item->title = 'Picture by ' . htmlentities( $uploader );
			$item->description = 'A new picture by ' . htmlentities( $uploader ) . ' at ' . $timestamp;
			$item->link = $file . '/';
			$item->guid = $file . '/';
			$item->pubDate = $timestamp;
			$rss_channel->items[] = $item;
		}
		$stmt->close();
		$db->close();
		$rss_feed = new rssGenerator_rss();
		$rss_feed->encoding = 'UTF-8';
		$rss_feed->version = '2.0';
		header('Content-Type: text/xml');
	 	$rss = $rss_feed->createFeed($rss_channel);

	 	phpFastCache::set( 'rss', $rss, 60 );
	 	echo $rss;
	} else {
		header('Content-Type: text/xml');
		echo $rss;
	}