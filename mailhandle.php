<?php
require_once 'config.php';
require_once 'alphaID.inc.php';
require_once 'PictureMailer.class.php';

if(isset( $_POST['sendemail'] ) && $_POST['sendemail'] === '1' 
	&& isset( $_POST['email'] ) && isset( $_POST['nick'] )
	&& isset( $_POST['imgids'] ) && isset( $_POST['delkey'] ) ) {
	setcookie( 'email', $_POST['email'], time() + 365 * 24 * 60 * 60, '/' );
	$mailer = new PictureMailer;
	$mailresult = $mailer->send_mail( $_POST['email'], $_POST['nick'], $_POST['imgids'], $_POST['delkey'] );
}

if( isset( $mailresult ) ) {
	switch ( $mailresult ) {
		case PictureMailer::SUCCESS :
			echo '<p class="alert alert-success">Email sent!</p>';
			break;
		case PictureMailer::ERROR_EMAIL_INVALID :
			echo '<p class="alert alert-error">Code 1001: Couldn\'t send email. Sorry!</p>';
			break;
		case PictureMailer::ERROR_OTHER :
			echo '<p class="alert alert-error">Code 1002: Couldn\'t send email. Sorry!</p>';
			break;
		default:
			echo '<p class="alert alert-error">Code 1003: Couldn\'t send email. Sorry!</p>';
			break;
	}
}
?>