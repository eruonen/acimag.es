<?php
require_once 'config.php';
require_once 'alphaID.inc.php';
if ( $_SERVER['REQUEST_METHOD'] === 'GET' || !isset( $_POST['nick'] ) || empty( $_POST['nick'] ) ) {
	header( 'Location: ' . SITE_URL );
	exit;
} else {
	setcookie( 'nick', $_POST['nick'], time() + 365 * 24 * 60 * 60 , '/');
}
?>
<!doctype html>
<html lang="en">
<head>
<title>Animal Crossing image uploader</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/acimages.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script> 
$(document).ready( function() {

	$('#mailbutton').click( function(e) {
		e.preventDefault();
		$.post('mailhandle.php',
			{ imgids : $('#imgids').val(), nick : $('#nick').val(), delkey : $('#delkey').val(), sendemail : $('#sendemail').val(), email : $('#email').val() },
			function( data ) {
				$('.span12').append( data );
			}
		);
	});
});
</script>
</head>
<body>
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<a class="logo" href="<?php echo SITE_URL; ?>"><h4>Animal Crossing image uploader</h4></a>
<?php
if( !isset( $_FILES ) || empty( $_FILES ) ) {
	echo '<div class="alert alert-error">Did you forget to pick an image?</div>';
} else {
	$htmlresult = '<p class="alert alert-success">Your image code(s) are '; 
	$deletekey = sha1( rand(122937, time() ) . htmlentities( $_POST['nick'], ENT_QUOTES ) );
	$deletekey = substr($deletekey, 0, 12);
	$image_pages = 'Images ';
	$filenames = array();
	$checked = false;
	for( $i = 0; $i < sizeof($_FILES['images']['name']); $i++ ) {
		if( isset( $_FILES['images']['name'][$i] ) && !empty( $_FILES['images']['name'][$i] ) ) {
			if( $_FILES['images']['error'][$i] > 0 ) {
				echo '<div class="alert alert-error">An error occurred when uploading.</div>';
				return;
			} else if( $_FILES['images']['type'][$i] !== 'image/jpeg' ) {
				echo '<div class="alert alert-error">Unsupported filetype! (jpg/jpeg only)</div>';
				return;
			} else if( $_FILES['images']['size'][$i] > 500000 ) {
				echo '<div class="alert alert-error">File size is too big! (500 KB max)<br>If you\'re uploading a legit 3DS image, please upload it elsewhere and send it to me so I can edit the max filesize.</div>';
				return;
			} else if( exif_imagetype( $_FILES['images']['tmp_name'][$i] ) != IMAGETYPE_JPEG ) {
	    		echo '<div class="alert alert-error">File doesn\'t seem to be an image.</div>';
	    		return;
	    	} else {
	    		$checked = true;
			}
		}	
	}
	if( $checked == true ) {
		for( $i = 0; $i < sizeof($_FILES['images']['name']); $i++ ) {
			if( isset( $_FILES['images']['name'][$i] ) && !empty( $_FILES['images']['name'][$i] ) ) {
				$imagesize = getimagesize( $_FILES['images']['tmp_name'][$i] );
				if( $imagesize[0] !== 400 && $imagesize[1] !== 240 ) {
					echo '<div class="alert alert-error">Image has to be an Animal Crossing screenshot!</div>';
					return;
				} else {
					$nick = $_POST['nick'];
					$db = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
					$stmt = $db->prepare( 'INSERT INTO photos(uploader, deletekey)  VALUES(?,?)' );
					$stmt->bind_param( 'ss', $nick, $deletekey );
					if($stmt->execute()) {
						$photo_id = $stmt->insert_id;
						$filename = alphaID( $photo_id, false, 4 );
						if( move_uploaded_file( $_FILES['images']['tmp_name'][$i], 'i/' . $filename . '.jpg' ) ){
							$image_page = SITE_URL . 'i/' . $filename . '/';
							$image_pages .= $image_page . ' ';
							$image_urls[] = SITE_URL . 'i/' . $filename . '.jpg';
							$filenames[] = $filename;
							$deletekey;
							$htmlresult .= '<a href="'  . $image_page . '"><strong>' . $filename . '</strong></a>, ';
						} else {
							echo '<div class="alert alert-error">Couldn\'t upload file. Check if directory is writeable!</div>';
							$stmt->close();
							$db->close();
							return;
						}
			    	}
			    	$stmt->close();
					$db->close();
				}
			}
		}
	}
	$htmlresult .= ' and your delete key is <strong>' . $deletekey . '</strong>. Click a link to see the image!</p>';
	echo $htmlresult;
?>
<form id="email-img" method="POST" action="<?php echo SITE_URL . 'mailhandle.php/'; ?>">
<input type="hidden" id="imgids" name="imgids" value="<?php echo htmlentities( serialize ( $filenames ) ); ?>">
<input type="hidden" id="nick" name="nick" value="<?php echo $nick; ?>">
<input type="hidden" id="delkey" name="delkey" value="<?php echo $deletekey; ?>">
<input type="hidden" id="sendemail" name="sendemail" value="1">
<div class="input-prepend">
<span class="add-on"><i class="icon-envelope"></i></span>
<input id="email" name="email" type="text" maxlength=120 placeholder="Email me this info" value="<?php if( isset( $_COOKIE['email'] ) ) echo $_COOKIE['email']; ?>" required>
</div>
<button id="mailbutton" type="submit" class="btn btn-primary">Send</button>
</form>
<?php
}
?>
<p style="display:none;" class="alert" id="email-result"></p>
</div>
</div>
</div>
</body>
</html>