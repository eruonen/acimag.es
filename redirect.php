<?php 
	require_once 'config.php';
	if ( ctype_alnum( $_GET['image'] ) && strlen($_GET['image']) == 4 ) {
		header('Location: ' . SITE_URL . 'i/' . $_GET['image'] . '/');
	} else {
		header('Location: ' . SITE_URL . 'index.php?error=1' );
	}
?>
