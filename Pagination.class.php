<?php
class Pagination {

	public static function generate_pagination( $current_page, $n_pages, $window, $base_url ) {
		// calculate start page
		$start_page = $current_page - ceil( $window / 2.0 ) + 1;
		if( $start_page > $n_pages - $window ) {
			$start_page = $n_pages - $window;
		}
		if( $start_page < 1 ) {
			$start_page = 1;
		}
		// calculate end page
		$end_page = $current_page + floor( $window / 2.0 );
		if( $end_page < $window ) {
			$end_page = $window;
		}
		if( $end_page > $n_pages ) {
			$end_page = $n_pages;
		}

		$html = '<ul>';
		$html .= '<li class="' . ($current_page == 1 ? 'active' : '') . '"><a href="' . $base_url . '&page=1">&laquo;</a></li>';
		$html .= '<li class="' . ($current_page == 1 ? 'active' : '') . '"><a href="' . $base_url . '&page=' . ( $current_page - 1 ) . '">&lsaquo;</a></li>';

		for ( $i = $start_page; $i <= $end_page; $i++ ) { 
			$html .= '<li class="' . ( $i == $current_page ? 'active' : '' ) . '">'; 
			$html .= '<a href="' . $base_url . '&page=' . $i .'">' . $i . '</a>';
			$html .= '</li>';
		}
		$html .= '<li class="' . ($current_page == $n_pages ? 'active' : '') . '"><a href="' . $base_url . '&page=' . ( $current_page + 1 ) . '">&rsaquo;</a></li>';
		$html .= '<li class="' . ($current_page == $n_pages ? 'active' : '') . '"><a href="' . $base_url . '&page=' . $n_pages . '">&raquo;</a></li>';
		$html .= '</ul>';
		return $html;
	}
}