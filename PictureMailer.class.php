<?php
require_once 'config.php';

class PictureMailer {
	const SUCCESS = 0;
	const ERROR_EMAIL_INVALID = 1;
	const ERROR_OTHER = 2;

	public $sender = 'noreply@acimag.es';

	public function send_mail( $email_address, $name, $filenames, $deletekey ) {
		if( filter_var( $email_address, FILTER_VALIDATE_EMAIL ) ) {
			$subject = 'Here\'s your Animal Crossing screenshot!';
			$body = "Dear $name, \n\nYour Animal Crossing screenshots can be found here:\n\n";
			for ( $i = 0; $i < sizeof( $filenames ); $i++ )
			{
				$body .= "Image $i :" . SITE_URL . 'i/' . $filenames[$i] . "/\n
				Delete this image by visiting this URL:\n\n" . SITE_URL . 'delete.php?imageid=' . $filenames[$i] . '&deletekey=' . $deletekey . "\n\n";
			}
			$body .= 'Thanks for using acimag.es!';
			$headers = 'From: ' . $this->sender;
			if( mail( $email_address, $subject, $body, $headers ) ) {
				return self::SUCCESS;
			} else {
				return self::ERROR_OTHER;
			}
		} else {
			return self::ERROR_EMAIL_INVALID;
		}
	}
}