<?php
/* Database info */
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', '3dsphotos');
/* Other */
define('SITE_URL', 'http://localhost/acimag.es/');
/* Debug makes uploading images from PC possible. */
define('DEBUG', false);